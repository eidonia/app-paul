package sleet.recepe.domain

import androidx.room.Database
import androidx.room.RoomDatabase
import sleet.recepe.providers.recepe.RecepeDao
import sleet.recepe.providers.recepe.RecepeEntity

@Database(entities = [RecepeEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun recepeDao(): RecepeDao
}

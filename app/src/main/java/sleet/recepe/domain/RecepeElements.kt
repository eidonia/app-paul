package sleet.recepe.domain

class RecepeElements {
    enum class DishType{
        DESSERT, PLAT, ENTREE, APERITIF, BOISSON
    }
    enum class Difficulty{
        FACILE, MOYEN, DIFFICILE, UNKNOWN
    }

    enum class Diet{
        OMNI, VEGETARIEN, VEGAN
    }
}
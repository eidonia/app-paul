package sleet.recepe.ui.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.unit.dp
import sleet.recepe.domain.RecepeElements
import sleet.recepe.ui.models.NewRecepeModel

class RecepeFormComposables {

    @Composable
    fun ingredientsForm(viewModel: NewRecepeModel) {
        Box( modifier = Modifier.border(width = 1.dp, color = Color.Black)
        ) {
            Column {
                Text(text = "ingrédients")
                var ingredientName by remember { mutableStateOf("nom") }
                var ingredientQuantity by remember { mutableStateOf("quantité") }
                val ingredientFakeMap: MutableMap<String, String> = remember { mutableStateMapOf() }

                TextField(
                    value = ingredientName,
                    onValueChange = { ingredientName = it },
                )
                Row {
                    TextField(
                        value = ingredientQuantity,
                        onValueChange = { ingredientQuantity = it },
                    )
                    Button(
                        onClick = {
                            if (!ingredientName.isBlank() && !ingredientQuantity.isBlank()) {
                                viewModel.ingredients[ingredientName] = ingredientQuantity
                                ingredientFakeMap[ingredientName] = ingredientQuantity
                                ingredientName = ""
                                ingredientQuantity = ""
                            }
                        },
                        enabled = true,
                        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
                        shape = MaterialTheme.shapes.medium,
                    ) {
                        Text(text = "ajouter")
                    }
                }
                for (ing in ingredientFakeMap) {
                    Row {
                        Text(text = "${ing.key}. quantité: ${ing.value}", modifier = Modifier.padding(end = 15.dp))
                        Button(onClick = {
                            viewModel.ingredients.remove(ing.key)
                            ingredientFakeMap.remove(ing.key)
                            ingredientName = ""
                            ingredientQuantity = ""
                        }) {
                            Text(text = "retirer")
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun tagsForm(viewModel: NewRecepeModel) {
        Box( modifier = Modifier.border(width = 1.dp, color = Color.Black)
        ) {
            Column {
                Text(text = "tags")
                var tagName by remember { mutableStateOf("nom") }
                val tagFakeList: MutableList<String> = remember { mutableStateListOf() }

                Row {
                    TextField(
                        value = tagName,
                        onValueChange = { tagName = it },
                    )
                    Button(
                        onClick = {
                            if (!tagName.isBlank()) {
                                viewModel.tags.add(tagName)
                                tagFakeList.add(tagName)
                                tagName = ""
                            }
                        },
                        enabled = true,
                        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
                        shape = MaterialTheme.shapes.medium,
                    ) {
                        Text(text = "ajouter")
                    }
                }
                for (tag in tagFakeList) {
                    Row {
                        Text(text = "$tag ", modifier = Modifier.padding(end = 15.dp))
                        Button(onClick = {
                            viewModel.tags.remove(tag)
                            tagFakeList.remove(tag)
                            tagName = ""
                        }) {
                            Text(text = "retirer")
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun <T> dropDownMenu(viewModel: NewRecepeModel, placeholder: String, types: Array<T>) {
        var dishType by remember { mutableStateOf(placeholder) }
        var showMenu by remember { mutableStateOf(false) }
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            elevation = AppBarDefaults.TopAppBarElevation,
            color = MaterialTheme.colors.primary
        ) {
            Row(modifier = Modifier.fillMaxWidth()) {
                Text(text=dishType)
                Box(modifier = Modifier.weight(1f)) {
                    IconButton(onClick = { showMenu = !showMenu }) {
                        Icon(Icons.Default.MoreVert, "")
                    }
                    DropdownMenu(
                        expanded = showMenu,
                        onDismissRequest = { showMenu = false },
                    ) {
                        types.forEach { label ->
                            DropdownMenuItem(onClick = {
                                dishType = label.toString()
                                addToViewModel(viewModel, label)
                                showMenu = false
                            }) {
                                Text(text = label.toString())
                            }
                        }
                    }
                }
            }
        }
    }

    private fun <T> addToViewModel(viewModel: NewRecepeModel, label: T) {
        when(label) {
            is RecepeElements.DishType -> viewModel.dishType= label
            is RecepeElements.Difficulty -> viewModel.difficulty= label
            is RecepeElements.Diet -> viewModel.diet = label
            else -> "do nothing"
        }
    }
}
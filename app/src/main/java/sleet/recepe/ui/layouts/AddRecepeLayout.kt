package sleet.recepe.ui.layouts

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import sleet.recepe.MainMenu
import sleet.recepe.domain.RecepeElements
import sleet.recepe.service.RecepeService
import sleet.recepe.ui.composables.RecepeFormComposables
import sleet.recepe.ui.models.NewRecepeModel
import sleet.recepe.ui.models.factories.NewRecepeModelFactory
import sleet.recepe.ui.theme.RecepeTheme

class AddRecepeLayout : ComponentActivity() {
    private lateinit var viewModel: NewRecepeModel
    private lateinit var viewModelFactory: NewRecepeModelFactory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelFactory = NewRecepeModelFactory()
        viewModel = ViewModelProvider(this,viewModelFactory).get(NewRecepeModel::class.java)
        setContent {
            RecepeTheme {
                val context = LocalContext.current
                Surface(color = MaterialTheme.colors.background) {
                    Column (
                        modifier = Modifier.fillMaxWidth(),
                        verticalArrangement = Arrangement.Center
                    ) {
                        newRecepeForm()

                        Button(
                            onClick = {
                                context.startActivity((Intent(context, MainMenu::class.java)))
                            },
                            enabled = true,
                            border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
                            shape = MaterialTheme.shapes.medium,
                        ) {
                            Text(text = "retour")
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun newRecepeForm() {
        var recepeName by remember { mutableStateOf("nouvelle recette") }
        var instructions by remember { mutableStateOf("instructions") }
        var timeInMinutes by remember { mutableStateOf("0") }
        val composables = RecepeFormComposables()
        TextField(
            value = recepeName,
            label = { Text(text = "nom") },
            onValueChange = {
                recepeName = it
                viewModel.name = it
            },
        )

        composables.ingredientsForm(viewModel)
        TextField(
            value = instructions,
            onValueChange = {
                instructions = it
                viewModel.instructions = it
            },
        )
        composables.dropDownMenu(viewModel,"type de recette", enumValues<RecepeElements.DishType>())
        composables.dropDownMenu(viewModel,"difficulté", enumValues<RecepeElements.Difficulty>())
        composables.dropDownMenu(viewModel,"régime", enumValues<RecepeElements.Diet>())

        composables.tagsForm(viewModel)

        TextField(
            value = timeInMinutes,
            modifier = Modifier.fillMaxWidth(),
            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
            onValueChange = {
                timeInMinutes = it
                kotlin.runCatching { viewModel.timeNeeded = it.toInt() }
            }
        )

        Button(
            onClick = {
                RecepeService().save()
                println(viewModel.name)
            },
            enabled = true,
            border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
            shape = MaterialTheme.shapes.medium,
        ) {
            Text(text = "enregistrer")
        }
    }


}


package sleet.recepe.ui.models

import androidx.lifecycle.ViewModel
import sleet.recepe.domain.RecepeElements

class NewRecepeModel () : ViewModel() {
    var name:String = "nouvelle recette"
    val ingredients: MutableMap<String, String> = mutableMapOf()
    var instructions: String = ""
    var dishType: RecepeElements.DishType = RecepeElements.DishType.PLAT
    var difficulty: RecepeElements.Difficulty = RecepeElements.Difficulty.UNKNOWN
    var diet: RecepeElements.Diet = RecepeElements.Diet.VEGETARIEN
    var timeNeeded: Int = 0
    val tags: MutableList<String> = arrayListOf()
}
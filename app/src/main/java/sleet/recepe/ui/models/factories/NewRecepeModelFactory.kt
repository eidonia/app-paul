package sleet.recepe.ui.models.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import sleet.recepe.ui.models.NewRecepeModel

class NewRecepeModelFactory() : ViewModelProvider.Factory{

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewRecepeModel::class.java)){
            return NewRecepeModel() as T
        }
        throw IllegalArgumentException("Unknown View Model Class in NewRecepeModelFactory")
    }


}

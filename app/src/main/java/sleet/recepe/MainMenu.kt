package sleet.recepe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sleet.recepe.ui.layouts.AddRecepeLayout
import sleet.recepe.ui.theme.RecepeTheme

class MainMenu : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RecepeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Column (
                        modifier = Modifier.fillMaxWidth(),
                        verticalArrangement = Arrangement.Center
                    ){
                        recepeListButton()
                        newRecepeButton()
                        searchBar()
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun recepeListButton() {
    Button(
        onClick = {},
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = "liste des recettes")
    }
}

//@Preview(showBackground = true)
@Composable
fun newRecepeButton() {
    val context = LocalContext.current
    Button(
        onClick = {
            start(context)
                  },
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = "nouvelle recette")
    }
}

fun start(context: Context) {
    context.startActivity((Intent(context , AddRecepeLayout::class.java)))
}

@Preview(showBackground = true)
@Composable
fun searchBar() {

    var text by remember { mutableStateOf("rechercher un recette") }
    TextField(
        value = text,
        onValueChange = { text = it }
    )
}

package sleet.recepe.providers.recepe

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RecepeDao {
    @Query("SELECT * FROM recepe")
    fun getAll(): List<RecepeEntity>

    @Insert
    fun insert(recepe: RecepeEntity)

    @Delete
    fun delete(recepe: RecepeEntity)

}
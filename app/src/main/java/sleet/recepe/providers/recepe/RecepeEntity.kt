package sleet.recepe.providers.recepe

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import sleet.recepe.domain.RecepeElements

@Entity(tableName = "recepe")
data class RecepeEntity (
    @PrimaryKey
    val uid: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "ingredients") val ingredients: String?, //MAP string string to DO
    @ColumnInfo(name = "instructions") val instructions: String?,
    @ColumnInfo(name = "dishType") val dishType: RecepeElements.DishType?,
    @ColumnInfo(name = "difficulty") val difficulty: RecepeElements.Difficulty?,
    @ColumnInfo(name = "diet") val diet: RecepeElements.Diet?,
    @ColumnInfo(name = "timeNeeded") val timeNeeded: Int?,
    @ColumnInfo(name = "tags") val tags: String? //arraylist TODO
)